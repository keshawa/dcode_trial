<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //Protect from mess assignments
    protected $fillable = ['title','description'];
}
