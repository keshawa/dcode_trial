@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">Task List</div>

                <div class="panel-body">


                    <table class="table">
                        <tr>
                            <th>Task Title</th>
                            <th>Description</th>
                        </tr>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>
                                    {{ link_to_route('home.edit','Edit',[$task->id],['class'=>'btn btn-primary']) }}
                                </td>
                                <td>
                                    {!! Form::open(array('route'=>['home.destroy',$task->id],'method'=>'DELETE')) !!}
                                        {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>

                        @endforeach
                    </table>

                </div>
            </div>
          <div>
              <a href="{{action('HomeController@create')}}"><button class="btn btn-success">Add New Task</button></a>
          </div>
        </div>
    </div>
</div>
@endsection
