<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="PHP MySQL" />
		<meta name="keywords" content="HTML5, PHP,MySQL" />
        
        <title>Login</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
        
            <div class="content">
                <div class="title m-b-md">
                    Login
                </div>

                <div class="form">
                    <?php
						session_start();
						//require_once ("settings.php"); //connection info
						echo "<form>";
							echo "<table>";
								echo "<tr>";
									echo"<td/>Email :</td>";
									echo"<td/><label><input type='email' name='email' placeholder='example@email.com'/></label></td>";
								echo "</tr>";
								echo "<tr>";
									echo"<td/>Password :</td>";
									echo"<td/><input type='password' name='password'/></td>";
								echo "</tr>";
								echo "<tr>";
									echo"<td/></td>";
									echo"<td/><label><input type='submit' value='OK'/></label>";
									echo"<label><input type='reset' value='Reset'/></label></td>";
								echo "</tr>";
							echo "</table>";
						echo "</form>";
					?>
                </div>
            </div>
        </div>
    </body>
</html>
