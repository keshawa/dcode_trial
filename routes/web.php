<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Home page route (welcome.blade.php)
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index');
Route::get('task','TaskController@index');

Route::resource('home', 'HomeController'); //important to get resource (functions to link in views as buttons
Route::resource('task', 'TaskController');


Route::get('/create',function(){
    return view('index');
});

//Route::get('/home', 'HomeController@create');


Route::get('users', ['uses' => 'UsersController@index']);
//Route::get('users/create', ['uses' => 'UsersController@create']);
Route::post('users', ['uses' => 'UsersController@store']);